/*Erik Toman Program_3_Stacks
 Program runs through everything and even builds without any warnings
 Everything works
 */

// DoubleStack.cpp
#include "DoubleStack.h"

// Default constructor, the stack is empty to start

DoubleStack::DoubleStack() {
    size_p = 0; 
    length = 20;
    usedA = 0; //starts array at 0
    usedB = 19; //ends array at 19
}
// Default destructor

DoubleStack::~DoubleStack() {
    //The destructor is unnecessary because I did not make a dynamic array
}

// Add "value" to the top of stack A

void DoubleStack::PushA(char value) {
    if (usedA + usedB == size_p || size_p == length) //helps prevent overflow
        throw "a fit";
    array [usedA] = value; //puts a value in a
    usedA++; //moves pointer to the next open slot, one to the right
    size_p++;
}

// Add "value" to the top of stack B

void DoubleStack::PushB(char value) {
    if (usedA + usedB == size_p || size_p == length) //helps prevent overflow
        throw "a fit";
    array [usedB] = value; /*pushes the value given by the main into the 
                            next open slot*/
    usedB--; /*moves "pointer" back a slot, because B started at the right
             hand side of the array*/
    size_p++;
}

// Remove and return the item on the top of stack A

char DoubleStack::PopA() {
    if (size_p == 0) //checks for potential underflow
        throw "a fit";
    usedA--; //Backs the array up a slot
    size_p--;
    return array [usedA]; //has the latest variable returned

}

// Remove and return the item on the top of stack B

char DoubleStack::PopB() {
    if (size_p == 0) //checks for potential underflow
        throw "a fit";
    usedB++; //You are taking away a spot from the array
    size_p--;
    return array [usedB];


}

// Return the item on the top of stack A

char DoubleStack::TopA() {
    if (usedA <= 0) //prevents looking in an illegitimate place
        throw "a fit";
    return array [usedA - 1]; /*returns the variable that's one of the "left"
                                of where usedA is "pointing"*/
}

// Return the item on the top of stack B

char DoubleStack::TopB() {
    if (usedB >= 19) //prevents looking in an illegitimate place
        throw "a fit";
    return array [usedB + 1]; /*returns the variable that's one of the "right"
                                of where usedB is "pointing"*/
}

// Return the number of items in the stack

unsigned int DoubleStack::size() {
    return size_p; //returns the size
}