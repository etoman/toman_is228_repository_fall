/*The code is mostly there, but I cannot perfect it. The program is crashing in the second
 default simulation that you had.  The string of parameters that you email to me works great
 and gives me a completely reasonable output of:

 The number of planes that took off in the simulated time => 1
The number of planes that have landed in the simulated time => 24
The number of planes that crashed => 29
This is the total wait time in takeOff queue => 0
The average time that the planes spent in the landing queue => 170.167
The number of planes that were left in the takeOff queue => 51
The number of planes that were left in the landing queue => 28
 
 So I am not completely sure where I am going wrong, but this is the final submit.
*/

// Airport.cpp

#include "Airport.h"
#include <queue>
#include <cstdlib>
#include <iostream>
using namespace std;

void Airport(
        int landingTime, // Time segments needed for one plane to land
        int takeoffTime, // Time segments needed for one plane to take off
        double arrivalProb, // Probability that a plane will arrive in
        // any given segment to the landing queue
        //between 0 and 1
        double takeoffProb, // Probability that a plane will arrive in
        // any given segment to the takeoff queue
        int maxTime, // Maximum number of time segments that a plane
        // can stay in the landing queue
         int simulationLength// Total number of time segs. to simulate
        ) {
    //there is a step that we need to do before calling rand

    //if ( rand() < RAND_MAX * arrival prob) //true arrivalProb "percent" of the time
    //{ new arrival added to queue } //you will check for this everytime (put in for loop)
    queue <int> landingQ; //queue 1
    queue <int> takeOff; //queue 2
    //DOUBLE planes in takeoff queue , planes in landing queue , arrival total time, departure
    //total time, average times for takeoff, average times for landing
    int time_left = 0, next = 0, num_landing = 0, num_takeOff = 0, landing_time_left = 0;
    int  crashed = 0, takeOff_extra = 0, landing_extra = 0, landing_wait_time = 0;
    unsigned int clock = 0;
    double takeOff_time_left = 0, takeOff_wait_time = 0, total_landing_wait_time = 0, total_takeOff_wait_time = 0, total_wait_time = 0;
    srand(time(NULL));

    //cout planes that took off and planes that landed

    //takeoffs, landings, crashes, 
    for (clock = 0; clock < simulationLength; clock++) //check if you're within the given time allotment
    {
        if (rand() < RAND_MAX * arrivalProb) /*checking when the airplanes 
                                                   arrive*/ {
            landingQ.push(clock); //needs to push which iteration of the for loop the particular plane arrived at
            //cout << "Plane arrived at " << clock << endl;
        }
        if (rand() < RAND_MAX * takeoffProb) {
            takeOff.push(clock); //needs to push which iteration of the for loop the particular plane arrived at
            //cout << "Plane takes off at " << clock << endl;
        }
        
        if (landing_time_left > 0 || takeOff_time_left > 0) //check if the runway is busy
        {
            if (landing_time_left > 0) //checks to see specify which part of the or the statement involved take off or landing
            {
            landing_time_left--; //decrements temp variable so plane doesn't spent too many segments landing
            }
            if (takeOff_time_left > 0)//checks to see specify which part of the or the statement involved take off or landing
            {
            takeOff_time_left--; //decrements temp variable so plane doesn't spent too many segments taking off
            }
            
            /*cout << "continuing after runway busy check" << endl;*/
            continue;
        }
        //cout << "This is landingQ.front() right before the crash counter in for loop" << landingQ.front() << endl;
        if (clock - landingQ.front() -1 == maxTime) //checks for crashes
        {
            cout << "A crash has occurred" << endl; //tells user a crash has occured
            crashed++; //increments crash counter      
        }

        if (!landingQ.empty()) //checks for plane ready to land, checks to make sure that you're not going to pop on an empty queue
        {
            landing_wait_time = clock - landingQ.front(); //calculates wait time
            total_landing_wait_time += landing_wait_time;
            cout << "There is a plane landing => " << landingQ.front() << endl;
            landing_time_left = landingTime; //puts landing temp (user defined) into a temp variable that will be easily reset
            landingQ.pop();
            num_landing++;
            //cout << "Number of planes landed " << num_landing << endl;
            
        } else if (!takeOff.empty()) //check for planes ready to take off, checks to make sure you're not going to pop on empty queue
        {
            takeOff_wait_time = clock - takeOff.front(); //calculates wait time
            total_takeOff_wait_time += takeOff_wait_time; //adds wait_time and keeps ticking total_wait_time to be divided by all planes landed
            cout << "There is a plane takingOff => " << takeOff.front() << endl; //tells user the variable of the plane that was pushed via clock always before .pop()
            takeOff_time_left = takeoffTime; //sets takeoffTime (user defined) to a temp variable
            takeOff.pop(); //pops off plane from takeOff queue
            num_takeOff++; //increments number of planes that took off
           // takeOff_time_left = maxTime - landingTime;
        } else {
            continue;
        }
    } //end of for loop
    cout << "This is what clock is equal to => " << clock << endl;
    do 
    {
        cout << "A crash has occurred" << endl; 
        crashed++; //keeps track of crashed planes
        landing_extra++; //keeps track of number of planes that were left in queue
        landingQ.pop(); //removes top variable from the landingQ queue
    } while ((clock - landingQ.front() > maxTime) && (!landingQ.empty()));
    /*clock is the total simulationTime, subtracting that from the front of the queue
    helps determine whether or not it has spent too long in the queue and if it
    has over stayed its welcome.  If it has, it must crash.  Checking for an
    empty landing Queue means that you won't attempt to pop in an empty queue.*/
    do
    {
        takeOff_extra++; //increments variable to keep track of number of leftover planes in takeOff queue
        takeOff.pop(); //pops top varible off the takeOff queue
    } while (!takeOff.empty()); //makes sure that you will not pop on an empty stack
    
    cout << "The number of planes that took off in the simulated time => " << num_takeOff << endl; //outputs to user how many planes took off
    cout << "The number of planes that have landed in the simulated time => " << num_landing << endl; //outputs to user how many planes landed
    cout << "The number of planes that crashed => " << crashed << endl; //outputs to user number of planes that crashed
    cout << "This is the total wait time in takeOff queue => " << total_takeOff_wait_time << endl; //outputs to user the total wait time in the takeOff queue
    cout << "The average time that the planes spent in the landing queue => " << total_landing_wait_time / num_landing << endl; //ouputs to user average time spent in the takeOff queue
    cout << "The number of planes that were left in the takeOff queue => " << takeOff_extra << endl; //ouputs to user how many planes were left in the takeOff queue
    cout << "The number of planes that were left in the landing queue => " << landing_extra << endl; //outputs to user how many planes were left in the landing queue
}