// Airport.cpp

#include "Airport.h"
#include <queue>
#include <cstdlib>
#include <iostream>
//USING MAX TIME INCORRECTLY
using namespace std;

void Airport(
        int landingTime, // Time segments needed for one plane to land
        int takeoffTime, // Time segments needed for one plane to take off
        double arrivalProb, // Probability that a plane will arrive in
        // any given segment to the landing queue
        //between 0 and 1
        double takeoffProb, // Probability that a plane will arrive in
        // any given segment to the takeoff queue
        int maxTime, // Maximum number of time segments that a plane
        // can stay in the landing queue
        int simulationLength// Total number of time segs. to simulate
        ) {
    //there is a step that we need to do before calling rand

    //if ( rand() < RAND_MAX * arrival prob) //true arrivalProb "percent" of the time
    //{ new arrival added to queue } //you will check for this everytime (put in for loop)
    queue <int> landingQ; //queue 1
    queue <int> takeOff; //queue 2
    //DOUBLE planes in takeoff queue , planes in landing queue , arrival total time, departure
    //total time, average times for takeoff, average times for landing
    int time_left = 0, next = 0, num_landing = 0, num_takeOff = 0, landing_time_left = 0;
    int  crashed = 0, landing_wait_time = 0;
    unsigned int clock = 0;
    double takeOff_time_left = 0, takeOff_wait_time = 0, total_landing_wait_time = 0, total_takeOff_wait_time = 0, total_wait_time = 0;
    srand(time(NULL));

    //cout planes that took off and planes that landed

    //takeoffs, landings, crashes, 
    for (clock = 0; clock < simulationLength; clock++) //check if you're within the given time allotment
    {
        if (rand() < RAND_MAX * arrivalProb) /*checking when the airplanes 
                                                   arrive*/ {
            landingQ.push(clock); //needs to push which iteration of the for loop the particular plane arrived at
            //cout << "Plane arrived at " << clock << endl;
        }
        if (rand() < RAND_MAX * takeoffProb) {
            takeOff.push(clock); //needs to push which iteration of the for loop the particular plane arrived at
            //cout << "Plane takes off at " << clock << endl;
        }

        if (time_left > 0)
      {
          time_left--;
          //continue;
      }
                
        if (landing_time_left > 0 || takeOff_time_left > 0) //check if the runway is busy
        {
            if (landing_time_left > 0){
            landing_time_left--;
            }
            if (takeOff_time_left > 0){
            takeOff_time_left--;
            }
            cout << "continuing after runway busy check" << endl;
            continue;
        }
        //cout << "This is landingQ.front() right before the crash counter in for loop" << landingQ.front() << endl;
        if (clock - landingQ.front() -1 == maxTime) //checks for crashes
        {
            cout << "crash increments in for loop" << endl; 
            //landingQ.pop();
            crashed++; //increments crash counter
            cout << "This is clock" << clock << endl;
           
        }

        if (!landingQ.empty()) //checks for plane ready to land
        {
            //make sure that the queue has values in it
            //clock - landingQ;
            landing_wait_time = clock - landingQ.front(); //calculates wait time
            total_landing_wait_time += landing_wait_time;
             cout << "landing_time_left = " << landing_time_left <<  " maxTime " << maxTime <<  " - landingQ.front(); " << landingQ.front() << endl;
            landing_time_left = landingQ.front() - maxTime;
            cout << "There is a plane landing at time => " << landingQ.front() << endl;
            landingQ.pop();
            num_landing++;
            //cout << "Number of planes landed " << num_landing << endl;
            
        } else if (!takeOff.empty()) {
            //average wait time += wait time
            cout << "takeOff_wait_time = clock - takeOff.front(); => " << takeOff_wait_time << " " << clock << " " << takeOff.front() << endl;
            takeOff_wait_time = clock - takeOff.front(); //calculates wait time
            total_takeOff_wait_time += takeOff_wait_time; //adds wait_time and keeps ticking total_wait_time to be divided by all planes landed
            cout << "There is a plane taking off at time => " << takeOff.front() << endl;
            takeOff.pop();            
            num_takeOff++;
            takeOff_time_left = maxTime - landingTime;
            //cout << "Number of planes that have departed " << num_takeOff << endl;
        } else {
            continue;
        }
    } //end of for loop
    cout << "This is what clock is equal to => " << clock << endl;
    do 
    {
        //cout << "crash increments after for loop " << endl;
        crashed++;
        landingQ.pop();
    } while ((clock - landingQ.front() > maxTime) && (!landingQ.empty()));
        
    cout << "The number of planes that took off in the simulated time => " << num_takeOff << endl;
    cout << "The number of planes that have landed in the simulated time => " << num_landing << endl;
    cout << "The number of planes that crashed => " << crashed << endl;
    cout << "This is the total wait time in takeOff queue => " << total_takeOff_wait_time << endl;
    cout << "The average time that the planes spent in the landing queue => " << total_landing_wait_time / num_landing << endl;
    //cout << "The number of planes that were left in the takeOff queue => " << << endl;
    //cout << "The number of planes that were left in the landing queue => " << << endl;
}

//cout << "The number of planes crashes " << crashed << endl;
//averages //average time spent in queue
//need to divide total_wait_time / total number of landed planes = average_wait_time
//time by the amount of planes that took off
//how many planes landed
//how many planes took off
//final summary of cout
/*
        //average_wait_time += wait_time;
        //time - top = time spent in queue

        //check if plane has already crashed

        //is the clock - the value that we are going to take out of the queue greater than the wait
        //time that we are going to take out of the clock
 */
/*
      if (!( time_left > 0) && !(landingQ.empty()))
        {
            next = landingQ.front();
            landingQ.pop();
            num_landing++;
            time_left = landingTime; 
        }
 
*/