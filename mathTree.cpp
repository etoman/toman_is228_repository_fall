// mathTree.cpp
#include <iostream>
#include <sstream>
#include "mathTree.h"
#include <string>
#include <cstdlib>
using namespace std;

string stringify(long double x) {
    ostringstream o;
    if (!(o << x))
        throw;
    return o.str();
}

//DONE
//constructor
mathTree::mathTree() {
    /*The default constructor contains the root pointer to NULL.  The copy 
     * constructor needs to make a new copy of the source's tree, and point 
     * root_ptr to the root of this copy.  Use the tree_copy function 
     * to do the copying. */

    root = NULL; //sets root to NULL
}

//DONE
//destructor
mathTree::~mathTree() {
    /* The destructor needs to return all nodes to the heap.  Again, you should
     make use of an appropriate function from bintree.h*/

    _deleteRecursive(root); //calls the delete function
    mathTree(); //runs the constructor
}

//DONE
//Write like a post order traversal that deletes
void mathTree::_deleteRecursive(treeNode* t) {

    if (t -> leafNode == false){
        throw "a baby";
    }
        
    if (t == NULL) { //checks if you're at roots
        return;
    } else {
        _deleteRecursive(t -> lChild);
        _deleteRecursive(t -> rChild);
        delete t; //deletes leafNode
    }
}

// Reads an expression in prefix notation & builds a tree appropriately
void mathTree::ReadExpression(string s) {
    
    root = new treeNode; //sets root to a new treeNode
    string leftover = _readex(root,s); //Reads the expressions and looks for spaces with the leftovers of the string
}

//DONE
//Reads the expression and looks for spaces //puts stuff into t
string mathTree::_readex(treeNode* t, string exp) {
    string firstpart;
    string remaining;
    
    if(t == NULL) //checks to see if the string is empty
    {
        throw "a baby";
    }
    
    if(exp.empty()) //checks to see if the string is empty
    {
        throw "a baby";
    }
    
    size_t pos = exp.find(' '); //find first space
    firstpart = exp.substr(0, pos); //creates substring for the first part up to but not including the space
    remaining = exp.substr(pos + 1); //creates substrings that holds everything past and not including the space
    
    if ((firstpart[0] == '+') || (firstpart [0] == '*')) { //checks for firstpart = op;
    t -> leafNode = false; //checks if the leafNode is false
    t -> op = exp[0];
    t -> lChild = new treeNode; //set to NULL by the constructor
    remaining = _readex (t -> lChild, remaining);
    t -> rChild = new treeNode; //set to NULL by the constructor
    remaining = _readex (t -> rChild, remaining);
    }
    else {
        t -> leafNode = true; //checks for leafNode
        double factor;
        factor = atof (firstpart.c_str()); //copies the string of first part if it's a variable
        t -> value = factor;
        t -> lChild = NULL;
        t -> rChild = NULL;
    }
    return remaining;
    }

// Returns the result of evaluate() on the root of the tree
double mathTree::ExpressionValue() {
    double answer = _evaluate(root);
    return answer;
}

//Returns the value of the expression at t
double mathTree::_evaluate(treeNode* t) 
{
    if (t -> leafNode == true)
    {
        return t -> value; 
    }
    
    else
    {
        double a = _evaluate(t -> lChild); //recursively calls evaluate until you have found the leftmost child
        double b = _evaluate(t -> rChild); //recursively calls evaluate until you have found the rightmost child
            
        if (t -> op == '+')
        { //checks for parent operator
            return a + b; //returns what Expression Value needs
        }
        
        if (t -> op == '*')
        { //checks for parent operator
            return a * b; //returns what Expression Value needs
        }
    }
}

/* Traverses the tree, returning an infix version of the expression as a string,
with parentheses around each expression */
string mathTree::ReturnInfix() {
    //look at expression value //it's the same thing but with a string
    string result = _inorder(root);
    return result;
}

string mathTree::_inorder(treeNode* t) {
    // LPR Left, Parent, Right
    
        //_inorder(/*treeNode * t*/) {
            string var; //makes a local string called var
            
            if (t -> leafNode == true){
                cout << t -> value << endl;
            }
            else {
                if (t -> lChild != NULL){
                    _inorder (t -> lChild);
                }
                
                cout << t -> op;
                    
                if (t-> rChild != NULL){
                    _inorder (t -> rChild);
                }
                    
            } 
           return var;     
                
}
     










//from page 504    
/*
if (node_ptr != NULL)
{
    inorder_print(node_ptr -> left ());
    std::cout << node_ptr->data() << std::endl;
    inorder_print(node_ptr->right());
}
 */



/*
 * Step 0 valid inputs if string - throw
 * if t is NULL throw
 
 * Step 1 seperate expt into remaining and 1st part
 
 * int pos = exp.find(" ");
 
 */

//Destructor
/* delete recursive (treeNode
        if it is a leaf node
            delete current node
     else   
            deleteRecursive (lchild)
            deleteRecursive (rchild)
     root = NULL;
 */
/*
if (t->leafNode == true) {
delete leafNode;
}
else {
    deleteRecursive (lChild);
    deleteRecursive (rChild);
}
 */

/*
    firstpart = exp;
    if ((firstpart [0] == '+') || (firstpart [0] == '*'))
    
    if (char [0] = op)
    {
        t -> leafNode = false;
        m -> op = t[0];
    }
     * */