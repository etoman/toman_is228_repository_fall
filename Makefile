CC = g++
CCFLAGS = -Wall -g

all: program5

clean: clean5

###PROGRAM 5#######################
program5: prog5/main.o mathTree.o Makefile
	${CC} ${CCFLAGS} -o program5 prog5/main.o mathTree.o

clean5:
	rm -f program5 prog5/main.o mathTree.o *.core

mathTree.o: mathTree.cpp mathTree.h Makefile
	${CC} ${CCFLAGS} -c mathTree.cpp
	
prog5/main.o: prog5/main.cpp mathTree.h Makefile
	${CC} ${CCFLAGS} -o prog5/main.o -c prog5/main.cpp

