// main.cpp

#include <iostream>
#include "../Airport.h"

using namespace std;

int main() {
    
    char x[128];
    
    cout << "Starting the first simulation." << endl;

   // Airport(5, 2, 0.1, 0.2, 45, 120);
Airport(20, 5, 0.1, 0.1, 100, 500);
    cout << "First simulation finished (press enter to continue)" << endl;

    cin.get(x[0]);

    cout << "Starting the second simulation." << endl;

    Airport(15, 10, 0.01, 0.01, 30, 1440);
//Airport(20, 5, 0.1, 0.1, 100, 500);
    cout << "Second simulation finished" << endl;
    
    return 0;
}

/*
void Airport(
  int landingTime,    // Time segments needed for one plane to land
  int takeoffTime,    // Time segs. needed for one plane to take off
  double arrivalProb, // Probability that a plane will arrive in
                      // any given segment to the landing queue
  double takeoffProb, // Probability that a plane will arrive in
                      // any given segment to the takeoff queue
  int maxTime,        // Maximum number of time segments that a plane
                      // can stay in the landing queue
  int simulationLength// Total number of time segs. to simulate
);
*/