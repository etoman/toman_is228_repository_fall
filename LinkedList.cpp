/*
 Received help from and helped Diane Baraw.  We both attempted to debug each others code and logically reason
 * as to why it was working the way it was.
 Problems with the Code:
 The insert function does not put the Nodes into the proper places in the Linked List which is leading to the outputs
 for the GetNode and Search being incorrect or garbage.
 
 Lessons Learned:
 Just because a code compiles DOESN'T MEAN IT'S RIGHT - but that makes since.  It just means no syntax errors.
 The big lesson was learning that "cout" tests don't necessarily mean that the code is working correctly, it just
 means that it is compiling up to and past that point
 Also that run successful or failed is not as big of a deal as it seems at first.*/
// LinkedList.cpp

#include "LinkedList.h"

// Default constructor, the list is empty to start

LinkedList::LinkedList() {
    length = 0; //sets length to 0
    head = NULL; //sets head to NULL
}

// Default destructor, must delete all nodes

LinkedList::~LinkedList() {
    if (head == NULL) //tells the program that you have hit the end of the list
    {
        throw "head == NULL"; //explains to the user that head == NULL
    }
    while (head != NULL) /*tells program you have not yet hit end of list and conversely
                           runs code to allow you to delete the rest of the nodes*/ {
        Node* remove; //makes new node called remove
        remove = head; //equals remove to head
        head = head -> next; //points head to next
        delete remove; //deletes remove
    }
}

// Add a node containing "value" to the front

void LinkedList::InsertFront(double value) {
    Node* fresh; //makes new Node pointer called fresh
    fresh = new Node; //makes fresh become the new front of the list
    fresh -> data = 0; //points fresh to data and then sets data to 0
    fresh -> next = head; //points fresh to next and sets next to head (beginning of the list)
    head = fresh; //sets head to fresh
}

// Add a node containing "value" to position "index"

void LinkedList::Insert(double value, unsigned int index) {

    Node *fresh, *prev; //given an index from up above

    fresh = new Node; //set fresh to new Node
    fresh -> data = value; //set data type to number stored in value
    prev = head; //sets prev to head
    for (int i = 0; i < length; i++) //steps the program incrementally through the list 
    {
        prev = prev -> next; //takes prev and points it to next
    }
    fresh -> next = prev -> next; //copies preceding node "next"
    //to new nodes "next"

    prev -> next = fresh; //sets prev to point to next and equals it to fresh

    length++; //This helps with getLength because it adds one to the length whenever you go through the function

    if (index > length) //statement to make sure that your are not running off the end of the list
    {
        cout << "this is index => " << index << endl;
        throw "Index is greater than length"; //this statement explains to the user what failed
    }

    //    This is the code from class that I just could not seem to get working without error, I know that it was the key
    //    to getting the Pointers in the right place but I just couldn't seem to find the problem

    /*
      Node *fresh, *prev; //given an index from up above
    cout << "Entered Insert" << endl;
    if (index < length) {
        throw "index is greater than length";
    }

    for (int i = 0; i < index - 1; i++) {
        prev = prev -> next;
    }

    fresh = new Node;
    fresh -> data = value;
    fresh -> next = prev -> next;
    prev -> next = fresh;
    length++;
     */
}

// Return value at position "index"

double LinkedList::GetNode(unsigned int index) {

    Node *cursor;
    if (index > length) //statement to make sure that your are not running off the end of the list
    {
        throw false;
    }

    for (cursor = head; cursor != NULL; cursor = cursor -> next) //steps through the GetNode statement to find correct Node
    {
        if (cursor -> data) //States data is the equivalent to length
        {
            return index; //this is the value that we are looking for
        }
    }
}

// Return the index of the node containing "value"

unsigned int LinkedList::Search(double value) { //double value is determined by
    //user

    Node *next = head; //creates new Node pointers and sets it so head

    for (int i = 0;; i++) //steps *next through linked list and helps search for Node
    {
        if (i > length) //forces program to not walk off end of list
        {
            throw "invalid length"; //tells user that something happened with the length
        }
        if (next == NULL) //looks for next being set to NULL which would mean program has hit end of list
        {
            throw "the next is null"; //tells user what they have hit the end of the list
        }
        if ((next -> data == value)) //Should find what we are looking 
            //for
        {
            return i; //returns the value that we are looking for
        }
        next = next -> next; //sets next to point to next
    }
}

// Delete the node at position "index", return the value that was there

double LinkedList::DeleteNode(unsigned int index) {
    Node *prev, *temp; //make two node pointer, prev and temp

    if (index > length) //statement to make sure that your are not running off the end of the list
    {
        throw "Index is greater than length"; //this statement explains to the user what failed
    }

    prev = head; //sets prev to head 

    for (unsigned int i = 0; i < index; i++) //will work through the nodes to find the correct node to delete
    {
        prev = prev -> next; //sets prev to point to next
        return i; //returns the value that is incrementing in the for loop and that we were looking for
    }
    temp = (prev -> next) -> next; //sets temp to point to next through prev
    delete prev -> next; //deletes prevs pointer to next
    prev -> next = temp; //sets prev pointing to next (makes the connection the last step deleted) and equals it to temp
}

// This function reverses the order of all nodes so the first node is now the
// last and the last is now the first (and all between). So a list containing 
// (4, 0, 2, 11, 29) would be (29, 11, 2, 0, 4) after Reverse() is called.

void LinkedList::Reverse() {
    //    if (head == NULL) {//if this head is at NULL, then that means that you have hit the end of the list and there is nothing
    //                       //left to "reverse"
    //    }
    //        return;
    //    }
    //    Node *prev, *next, *cursor;
    //    
    //from here you would make a loop making sure that current is not equal to NULL
    //you would set the cursor to the prev pointer 
    //from there you would set the prev pointer to the cursor
    //this loops until you hit NULL
    //then you should have a reversed function

    // Return the number of nodes in the list
}

int LinkedList::GetLength() {
    {
        return length; /*very simple, just need to ensure that you are properly referencing and
    incrementing length throughout the rest of the code*/
    }
}